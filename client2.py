import pirc
import interface
import curses
import time
import sys

display_text = "Word: "
bad_guesses = ""


# Host and port
HOST = "irc.patwic.com"
PORT = 6667


# IRC server password
PASSWORD = "foobar"

# Create IRC server connection object
irc = pirc.IRC(HOST, PORT, PASSWORD)
# User data
NICK = "Sourcearray2"
REALNAME = "Sourcearray"

# Connect to server and register user
irc.connect(NICK, REALNAME)

# Channel
CHAN = "#patwic"

# Join channel
irc.send("JOIN " + CHAN)


def main(stdscreen):
    global NICK
    ui = interface.TextUI(stdscreen)
    irc.send(NICK + " has joined the chat")
    while True:
        clean_user_message = ui.input()
        if clean_user_message:
            send_message = None
            output_string = None
            if clean_user_message.startswith('/name '):
                send_message = 'NICK ' + clean_user_message[6:]
                NICK = clean_user_message[6:]
                output_string = 'NICK changed to ' + clean_user_message[6:]
            elif clean_user_message.startswith('/exit'):
                send_message = 'QUIT'
                sys.exit()
            elif clean_user_message.startswith('/hangman word '):
                secret_word = clean_user_message[14:]
                send_message = "PRIVMSG #patwic :" + NICK + ' has chosen a secret word. Let the game begin. Use "/hangman guess [letter]" to play.'
                output_string = "You have chosen a secret word succesfully"
            else:
                send_message = "PRIVMSG #patwic :" + clean_user_message
                output_string = NICK + ': ' + clean_user_message
                
            if send_message:
                irc.send(send_message)
            if output_string:
                ui.output(output_string)

                
        msg = irc.read()
        if msg:
            user_ends = msg.find('!')
            user = msg[1:user_ends]
            message_start = msg.find(':', 1) + 1
            clean_message = msg[message_start:]
            
            ui.output(user + ': ' + clean_message)

            if clean_message.lower() == '/hangman start':
                ui.output("Choose your secret word, please. ('/hangman word ')")
                
                
            
def menu(NICK, CHAN):
    menu_input = input("Do you wanna enter the chat? Y/N: ")
    if menu_input == "Y":
        irc.connect(NICK, REALNAME)
        irc.send("JOIN " + CHAN)
        curses.wrapper(main)
    elif menu_input == "N":
        print("OK, see you")
        return
    else:
        ("Invalid argument, you have been returned to the main menu")
        time.sleep(1)
        menu(NICK, CHAN)
        
def hangman():
    ui.output("Do you want to run a  hangman game? If so write a secret word, if you dont want to play, just simply write 'N'")
    
def again():
    irc.send("PRIVMSG #patwic :" + "Do you want to play again? Write [/hangman again] if you want to play again.")
    if irc.read() == "/hangman again":
        return menuhangman()
    else:
        return curses_wrapper(main)
        

def loss():
    return "You lost D:"
    again()
    
def win():
    return "You guessed all the letters, congratulations :D"
    again()

def display_word(correct_word, guess, guesses):
    global display_text
    global bad_guesses
    loss_message = ""
    display_text = "Word: "
    if guess not in correct_word:
        bad_guesses = bad_guesses + "a"
        if bad_guesses == "aaaaaaaaaa":
            loss_message = " You lost! :("
        else:
            idontknowwhyideclaredthisvariable = "Your mom is a donkey"
    else:
        bad_guesses = bad_guesses + ""

        for i in correct_word:
            if i in guesses:
                display_text = display_text + i
            else:
                display_text = display_text + "*"
    displaylol = display_text + "  You have " + str(10 - len(bad_guesses)) + " guesses left."
    displaylol = displaylol + loss_message
    return displaylol

    

def guess(secret_word):
    guesses_in_guess = ""
    while display_text[6:] != secret_word:
        if irc.read().startswith("/hangman guess "):
            if len(irc.read()[15:]) == 1:
                global_guess = irc.read()[15:]
            else:
                return guess(secret_word)
                    
            guesses_in_guess = global_guess + guesses_in_guess
            displayha = display_word(secret_word, global_guess, guesses_in_guess)
            irc.send("PRIVMSG #patwic :" + displayha)
        if "You lost" in displayha:
            return
    irc.send("PRIVMSG #patwic :" + win())


def menuhangman(stdscreen):
    your_mom = True
    while your_mom == True:    
        secret_word = ui.input()
        if len(secret_word) > 20:
            ui.output("Your word is too long, it may be max 20 letters")
            your_mom = True
        else:
            your_mom = False
            guess(secret_word, stdscreen)


    
#menu(NICK, CHAN)
curses.wrapper(main)
