import pirc
import interface
import curses
import time

display_text = "Word: "
bad_guesses = ""


# Host and port
HOST = "irc.patwic.com"
PORT = 6667


# IRC server password
PASSWORD = "foobar"

# Create IRC server connection object
irc = pirc.IRC(HOST, PORT, PASSWORD)
# User data
NICK = "Sourcearray"
REALNAME = "Sourcearray"

# Connect to server and register user
irc.connect(NICK, REALNAME)

# Channel
CHAN = "#patwic"

# Join channel
irc.send("JOIN " + CHAN)


def main(stdscreen):
    ui = interface.TextUI(stdscreen)
    NICK = "Sourcearray"
    CHAN = "#patwic"
    irc.send(NICK + " has joined the chat")
    while True:
        old_user_msg = ui.input()
        if old_user_msg:
            user_msg = "PRIVMSG #patwic :" + old_user_msg
            if old_user_msg.startswith("/name "):
                NICK = old_user_msg[6:]
                ui.output("Name changed")
            elif old_user_msg == "/exit":
               ui.output("Exiting channel in 3 seconds")
               time.sleep(3)
               irc.send(NICK + " has left the chat")
               return
                
        msg = irc.read()
        if msg:
            old_msglenvar1 = msg.find(":")
            msglenvar1 = old_msglenvar1 + 1
            msglenvar2 = msg.find("!")
            new_msg_part1 = msg[msglenvar1:msglenvar2]
            msglenvar1_ = msg.find(":", 1)
            new_msg_part2 = msg[msglenvar1_:]
            new_msg = new_msg_part1 + new_msg_part2
            ui.output(new_msg)
            new_msg_index1 = new_msg.find(":")
            hangman_msg = new_msg[new_msg_index1:]
        if old_user_msg:
            ui.output(NICK + ": " + old_user_msg)
            irc.send(user_msg)
        if hangman_msg == "/hangman start":
            return hangman()
        
            
def menu(NICK, CHAN):
    menu_input = input("Do you wanna enter the chat? Y/N: ")
    if menu_input == "Y":
        irc.connect(NICK, REALNAME)
        irc.send("JOIN " + CHAN)
        curses.wrapper(main)
    elif menu_input == "N":
        print("OK, see you")
        return
    else:
        ("Invalid argument, you have been returned to the main menu")
        time.sleep(1)
        menu(NICK, CHAN)
        
def hangman():
    ui.output("Do you want to run a  hangman game? If so write a secret word, if you dont want to play, just simply write 'N'")
    
def again():
    irc.send("PRIVMSG #patwic :" + "Do you want to play again? Write [/hangman again] if you want to play again.")
    if irc.read() == "/hangman again":
        return menuhangman()
    else:
        return curses_wrapper(main)
        

def loss():
    return "You lost D:"
    again()
    
def win():
    return "You guessed all the letters, congratulations :D"
    again()

def display_word(correct_word, guess, guesses):
    global display_text
    global bad_guesses
    loss_message = ""
    display_text = "Word: "
    if guess not in correct_word:
        bad_guesses = bad_guesses + "a"
        if bad_guesses == "aaaaaaaaaa":
            loss_message = " You lost! :("
        else:
            idontknowwhyideclaredthisvariable = "Your mom is a donkey"
    else:
        bad_guesses = bad_guesses + ""

        for i in correct_word:
            if i in guesses:
                display_text = display_text + i
            else:
                display_text = display_text + "*"
    displaylol = display_text + "  You have " + str(10 - len(bad_guesses)) + " guesses left."
    displaylol = displaylol + loss_message
    return displaylol

    

def guess(secret_word):
    guesses_in_guess = ""
    while display_text[6:] != secret_word:
        if irc.read().startswith("/hangman guess "):
            if len(irc.read()[15:]) == 1:
                global_guess = irc.read()[15:]
            else:
                return guess(secret_word)
                    
            guesses_in_guess = global_guess + guesses_in_guess
            displayha = display_word(secret_word, global_guess, guesses_in_guess)
            irc.send("PRIVMSG #patwic :" + displayha)
        if "You lost" in displayha:
            return
    irc.send("PRIVMSG #patwic :" + win())


def menuhangman(stdscreen):
    your_mom = True
    while your_mom == True:    
        secret_word = ui.input()
        if len(secret_word) > 20:
            ui.output("Your word is too long, it may be max 20 letters")
            your_mom = True
        else:
            your_mom = False
            guess(secret_word, stdscreen)


            
        
def settings():
    NAME = "Cardoland"
    CHAN = "#patwic"
    print("Type ´/help´ for commands, type other commands for other stuff to happen :P")
    settings_input = input(">")
    if "/help" == settings_input:
        print("/name - Changes your username in the PIRC")
        print("/channel - Changes channel")
        print("/exit - Goes back to the main menu")
        time.sleep(2)
        settings()
    elif "/name" == settings_input:
        NAME = input("New name: ")
    elif "/chan" == settings_input:
        CHAN = input("New channel: ")
        menu(NICK, CHAN)
    elif "/exit" == settings_input:
        time.sleep(1)
        menu(NICK, CHAN)
    else:
        print("Unknown command " + settings_input) 
        settings()
    
    
menu(NICK, CHAN)
